package com.service;

import com.model.User;

/**
 * @author xiasq
 * @version DemoService, v0.1 2018/1/11 22:52
 */
public interface DemoService {
    boolean doRegister(String username, String password);

    User findByUsername(String username);
}
