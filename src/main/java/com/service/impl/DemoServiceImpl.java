package com.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mapper.UserMapper;
import com.model.User;
import com.service.DemoService;

/**
 * @author xiasq
 * @version DemoServiceImpl, v0.1 2018/1/11 22:52
 */

@Service
public class DemoServiceImpl implements DemoService {
	private Logger logger = LoggerFactory.getLogger(DemoServiceImpl.class);

	@Autowired
	private UserMapper userMapper;

	@Override
	public boolean doRegister(String username, String password) {
		logger.info("service is running, userame is :{}, pasword is :{}", username, password);
		User user = new User(username, password);
		return userMapper.insertSelective(user);
	}

	@Override
	public User findByUsername(String username) {
		return userMapper.findByUsername(username);
	}
}
