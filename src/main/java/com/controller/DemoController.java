package com.controller;

import com.alibaba.fastjson.JSONObject;
import com.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.DemoService;

/**
 * @author xiasq
 * @version DemoController, v0.1 2018/1/11 22:19
 */

@RestController
@RequestMapping("/controller")
public class DemoController {
	private Logger logger = LoggerFactory.getLogger(DemoController.class);

	@Autowired
	private DemoService demoService;

	@RequestMapping("/register/{username}/{password}")
	public String doRegister(@PathVariable String username, @PathVariable String password) {
		logger.info("doRegister run...username is :{}, password is :{}", username, password);
		demoService.doRegister(username, password);
		return "register success";
	}

	@RequestMapping("/findByUsername/{username}")
	public String findByUsername(@PathVariable String username) {
		logger.info("findByUsername run...username is :{}", username);
		User user  = demoService.findByUsername(username);
		return JSONObject.toJSONString(user);
	}


}
