package com.mapper;

import com.model.User;
import org.apache.ibatis.annotations.Param;

/**
 * @author xiasq
 * @version UserMapper, v0.1 2018/1/11 23:02
 */

public interface UserMapper {

	boolean insertSelective(User user);

	User findAll();

	User findByUserId(@Param("id") long userId);

    User findByUsername(String username);
}
